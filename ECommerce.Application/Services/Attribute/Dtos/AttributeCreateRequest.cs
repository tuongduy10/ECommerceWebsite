﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Services.Attribute.Dtos
{
    public class AttributeCreateRequest
    {
        public string AttributeName { get; set; }

    }
}