﻿using ECommerce.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Services.Attribute.Dtos
{
    public class AttributeModel
    {
        public int AttributeId { get; set; }
        public string AttributeName { get; set; }

    }
}