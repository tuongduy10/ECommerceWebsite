﻿using ECommerce.Application.Services.Role.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Services.Role
{

    public class RoleService : IRoleService
    {
        public async Task<int> Create(RoleCreateRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Delete(int AttributeId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<RoleModel>> getAll()
        {
            throw new NotImplementedException();
        }

        public async Task<int> Update(RoleModel request)
        {
            throw new NotImplementedException();
        }
    }
}
