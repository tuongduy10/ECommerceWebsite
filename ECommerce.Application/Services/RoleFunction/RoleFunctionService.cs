﻿using ECommerce.Application.Services.RoleFunction.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Services.RoleFunction
{
    public class RoleFunctionService : IRoleFunctionService
    {
        public async Task<int> Create(RoleFunctionCreateRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Delete(int RoleFunctionId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<RoleFunctionModel>> getAll()
        {
            throw new NotImplementedException();
        }

        public async Task<int> Update(RoleFunctionModel request)
        {
            throw new NotImplementedException();
        }
    }
}
