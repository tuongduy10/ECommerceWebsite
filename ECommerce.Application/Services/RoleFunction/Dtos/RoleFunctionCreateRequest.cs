﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Application.Services.RoleFunction.Dtos
{
    public class RoleFunctionCreateRequest
    {
        public string RoleFunctionName { get; set; }
    }
}
